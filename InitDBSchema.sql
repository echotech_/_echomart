﻿CREATE TABLE [dbo].[Categories] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (100) NOT NULL,
    [IsDeleted] BIT            DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    UNIQUE NONCLUSTERED ([Name] ASC)
);

CREATE TABLE [dbo].[Products] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (100)  NOT NULL,
    [Description] NVARCHAR (2000) NOT NULL,
    [CategoryId]  INT             NOT NULL,
    [IsDeleted]   BIT             DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    UNIQUE NONCLUSTERED ([Name] ASC),
    UNIQUE NONCLUSTERED ([Name] ASC),
    CONSTRAINT [FK_Products_Categories] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Categories] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO [dbo].[Categories] ([Name], [IsDeleted]) VALUES (N'Food (Makanan)', 0)
INSERT INTO [dbo].[Categories] ([Name], [IsDeleted]) VALUES (N'Drink (minuman)', 0)
INSERT INTO [dbo].[Categories] ([Name], [IsDeleted]) VALUES (N'Stationery', 0)
INSERT INTO [dbo].[Categories] ([Name], [IsDeleted]) VALUES (N'Medicine', 0)

INSERT INTO [dbo].[Products] ([Name], [Description], [CategoryId], [IsDeleted]) VALUES (N'Gardenia', N'Bread. Rare. (Roti putih berkhasiat)', 1, 0)
INSERT INTO [dbo].[Products] ([Name], [Description], [CategoryId], [IsDeleted]) VALUES (N'GoodDay', N'Milk. Contains calcium.', 2, 0)
INSERT INTO [dbo].[Products] ([Name], [Description], [CategoryId], [IsDeleted]) VALUES (N'Pilot Pen', N'Pen by pilot, for writing.', 3, 0)
INSERT INTO [dbo].[Products] ([Name], [Description], [CategoryId], [IsDeleted]) VALUES (N'Panadol', N'Ubat demam', 4, 0)
INSERT INTO [dbo].[Products] ([Name], [Description], [CategoryId], [IsDeleted]) VALUES (N'Pencil Case', N'Kotak pensel?', 3, 0)
INSERT INTO [dbo].[Products] ([Name], [Description], [CategoryId], [IsDeleted]) VALUES (N'Eraser', N'Pemadam', 3, 0)