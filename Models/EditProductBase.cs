﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using EchoMart.Data;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EchoMart.Models
{
    public class EditProductBase
    {
        [Required, StringLength(100)]
        public string Name { get; set; }
        [Required, StringLength(2000)]
        public string Description { get; set; }
        [Required, DisplayName("Category")]
        public int CategoryId { get; set; }
        
        public IEnumerable<SelectListItem> Categories { get; set; }
    }
}
