﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EchoMart.Data;
using EchoMart.Models;

namespace EchoMart
{
    public class HomeService
    {
        readonly AppDbContext _context;
    
        public HomeService(AppDbContext context)
        {
            _context = context;
        }

        // Read
        public ICollection<ProductViewModel> GetProducts()
        {
            // Get list of categories for its names
            List<Category> CategoryList = _context.Categories
                .OrderBy(x => x.Id)
                .Select(x => new Category
                {
                    Id = x.Id, 
                    Name = x.Name
                })
                .ToList();
            var CategoryNames = CategoryList.ToDictionary(x => x.Id, x => x.Name);

            return _context.Products
                .OrderBy(x => x.Id)
                .Where(x => !x.IsDeleted)
                .Select(x => new ProductViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    CategoryName = CategoryNames[x.CategoryId]
                })
                .ToList();
        }

        public ICollection<CategoryViewModel> GetCategories()
        {
            return _context.Categories
                .OrderBy(x => x.Id)
                .Select(x => new CategoryViewModel
                {
                    Id = x.Id,
                    Name = x.Name
                })
                .ToList();
            
        }

        public UpdateProductCommand GetProductForUpdate(int productId)
        {
            return _context.Products
                .Where(x => x.Id == productId)
                .Select(x => new UpdateProductCommand
                {
                    Name = x.Name,
                    Description = x.Description,
                    CategoryId = x.CategoryId
                })
                .SingleOrDefault();
        }

        public UpdateCategoryCommand GetCategoryForUpdate(int categoryId)
        {
            return _context.Categories
                .Where(x => x.Id == categoryId)
                .Select(x => new UpdateCategoryCommand
                {
                    Name = x.Name
                })
                .SingleOrDefault();
        }



        // Create
        public void CreateProduct(CreateProductCommand cmd)
        {
            var product = cmd.ToProduct();
            _context.Add(product);
            _context.SaveChanges();
        }

        public void CreateCategory(CreateCategoryCommand cmd)
        {
            var category = cmd.ToCategory();
            _context.Add(category);
            _context.SaveChanges();
        }


        // Update
        public void UpdateProduct(UpdateProductCommand cmd)
        {
            var product = _context.Products.Find(cmd.Id);
            if (product == null)
            {
                throw new Exception("Unable to find the product");
            }

            cmd.UpdateProduct(product);
            _context.SaveChanges();
        }

        public void UpdateCategory(UpdateCategoryCommand cmd)
        {
            var category = _context.Categories.Find(cmd.Id);
            if (category == null)
            {
                throw new Exception("Unable to find the category");
            }

            cmd.UpdateCategory(category);
            _context.SaveChanges();
        }


        // Delete
        public void DeleteProduct(int productId)
        {
            var product = _context.Products.Find(productId);
            if (product.IsDeleted) { throw new Exception("Unable to delete a deleted product?"); }

            product.IsDeleted = true;

            /* 
            // For real remove, not recommended
            _context.Remove(product);
            */
            _context.SaveChanges();
        }
    }
}
