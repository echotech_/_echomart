﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using EchoMart.Data;

namespace EchoMart.Models
{
    public class UpdateProductCommand : EditProductBase
    {
        public int Id { get; set; }

        public void UpdateProduct(Product product)
        {
            product.Name = Name;
            product.Description = Description;
            product.CategoryId = CategoryId;
        }
    }
}
