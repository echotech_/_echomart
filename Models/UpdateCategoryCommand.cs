﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using EchoMart.Data;

namespace EchoMart.Models
{
    public class UpdateCategoryCommand : EditCategoryBase
    {
        public int Id { get; set; }

        public void UpdateCategory(Category category)
        {
            category.Name = Name;
        }
    }
}
