﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using EchoMart.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EchoMart.Controllers
{
    public class HomeController : Controller
    {
        public HomeService _service;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, HomeService service)
        {
            _logger = logger;
            _service = service;
        }

        // Read
        public IActionResult Index()
        {
            var models = _service.GetProducts();
            return View(models);
        }

        public IActionResult Categories()
        {
            var models = _service.GetCategories();
            return View(models);
        }


        // Create
        [HttpGet]
        public IActionResult CreateProduct()
        {
            return View(new CreateProductCommand 
            {
                Categories = GetSelectListCategories()
            });
        }

        [HttpPost]
        public IActionResult CreateProduct(CreateProductCommand command)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _service.CreateProduct(command);
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception)
            {
                // TODO: Log error
                // Add a model-level error by using an empty string key
                ModelState.AddModelError(
                    string.Empty,
                    "An error occured saving the product"
                    );
            }
            //If we got to here, something went wrong
            command.Categories = GetSelectListCategories();
            return View(command);
        }
 
        [HttpGet]
        public IActionResult CreateCategory()
        {
            return View(new CreateCategoryCommand());
        }

        [HttpPost]
        public IActionResult CreateCategory(CreateCategoryCommand command)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _service.CreateCategory(command);
                    return RedirectToAction(nameof(Categories));
                }
            }
            catch (Exception)
            {
                // TODO: Log error
                // Add a model-level error by using an empty string key
                ModelState.AddModelError(
                    string.Empty,
                    "An error occured saving the category"
                    );
            }
            //If we got to here, something went wrong
            return View(command);
        }


        // Update
        [HttpGet]
        public IActionResult EditProduct(int id)
        {
            var model = _service.GetProductForUpdate(id);
            if (model == null)
            {
                return NotFound();
            }
            model.Categories = GetSelectListCategories();
            return View(model);
        }

        [HttpPost]
        public IActionResult EditProduct(UpdateProductCommand command)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _service.UpdateProduct(command);
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError(
                    string.Empty,
                    "An error occurred saving the product"
                );
            }

            // If error
            command.Categories = GetSelectListCategories();
            return View(command);
        }

        [HttpGet]
        public IActionResult EditCategory(int id)
        {
            var model = _service.GetCategoryForUpdate(id);
            if (model == null)
            {
                return NotFound();
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult EditCategory(UpdateCategoryCommand command)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _service.UpdateCategory(command);
                    return RedirectToAction(nameof(Categories));
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError(
                    string.Empty,
                    "An error occurred saving the category"
                );
            }

            // If error
            return View(command);
        }

        // Delete
        [HttpPost]
        public IActionResult DeleteProduct(int id)
        {
            _service.DeleteProduct(id);

            return RedirectToAction(nameof(Index));
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private IEnumerable<SelectListItem> GetSelectListCategories()
        {
            return _service.GetCategories()
                .Select(item => new SelectListItem
                {
                    Value = item.Id.ToString(),
                    Text = item.Name
                });
        }
    }
}
