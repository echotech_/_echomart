﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using EchoMart.Data;

namespace EchoMart.Models
{
    public class CreateProductCommand : EditProductBase
    {
        public Product ToProduct()
        {
            return new Product
            {
                Name = Name,
                Description = Description,
                CategoryId = CategoryId
            };
        }
    }
}
