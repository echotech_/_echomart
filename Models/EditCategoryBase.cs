﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using EchoMart.Data;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EchoMart.Models
{
    public class EditCategoryBase
    {
        [Required, StringLength(100), DisplayName("Category")]
        public string Name { get; set; }
    }
}
