# How-to
## Clone to your local repository
Using VS, on landing page, click clone, and enter the git repo URL:  https://gitlab.com/echotech_/_echomart.git
### Manual
1. Open visual studio, identify your repo directory. It is usually at 
```
C:\Users\[your name]\source\repos
```
You can open any solution (with default configurations) and click Tools > CommandLine > DevCmdPrompt.  
cd to parent directory for the location of repos.

2. Assuming you are at the same directory on terminal, you can clone this project (solution) to your local repo:
```
git clone https://gitlab.com/echotech_/_echomart.git
```
3. That should settle it, any problem pls notify.

## Restore packages
1. Open DevCmdPrompt in the cloned solution.
2. Enter the command below to restore the package.
```
dotnet restore
```
3. This should restore all NuGet packages. Again, any problem pls notify.

## Initialize DB schema
1. Navigate to View > SQL Server Object Explorer
2. Its window should appear, click on SQL Server > (localdb)\MSSQLLocalDB... (any localdb is fine) > Databases.
3. On "Databases", right-click and choose Add New Database. Enter any name and confirm to create.
4. Refresh the SQL Server Object Explorer by clicking the refresh icon on top-left corner of the window.
5. On the newly-created database, right click and choose "New Query".
6. Open the "InitDBSchema.sql" in the solution root directory, copy the script and paste it into the "New Query" tab opened.
7. Execute. It should work. Again, any problem pls notify.

## Update DB connection string
1. Refresh SQL Server Object Explorer.
2. Navigate to your database that you have just created on the steps above.
3. Righ-click on the database and choose properties.
4. Find the "Connection String" row, copy the value.
5. In the "Solution Explorer" root directory, open appsettings.json
6. Replace the value of Data.Test.ConnectionString to the value you just copied.
7. Save.

## That should be all?
1. Start Debugging.
2. Enjoy.